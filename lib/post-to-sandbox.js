'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _formPost = require('./form-post');

var _formPost2 = _interopRequireDefault(_formPost);

var _depend = require('./depend');

var _depend2 = _interopRequireDefault(_depend);

function postToJsFiddle(sources) {
    var jsFiddleData = {
        wrap: 'd'
    };

    if (sources.html) {
        var jsFiddleExternalDependencies = _depend2['default'].renderHeadBlock(sources.dependencies);
        jsFiddleData.html = jsFiddleExternalDependencies + '\n' + sources.html;
    }

    if (sources.css) {
        jsFiddleData.css = sources.css;
    }

    if (sources.js) {
        jsFiddleData.js = sources.js;
    }

    (0, _formPost2['default'])('https://jsfiddle.net/api/post/library/pure/', jsFiddleData);
}

function postToJsBin(sources) {
    var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    var jsBinData = {};

    var panelsToDisplay = ['output'];

    if (sources.html) {
        jsBinData.html = _depend2['default'].wrapContentInDependencies(sources.html, sources.dependencies);
        panelsToDisplay.push('html');
    } else {
        jsBinData.html = _depend2['default'].wrapContentInDependencies('', sources.dependencies);
    }

    if (sources.css) {
        jsBinData.css = sources.css;
        panelsToDisplay.push('css');
    }

    if (sources.js) {
        jsBinData.javascript = sources.js;
        panelsToDisplay.push('js');
    }

    if (config && config.authToken) {
        jsBinData.api_key = config.authToken;
    }

    (0, _formPost2['default'])('https://jsbin.com/api/save', jsBinData);
}

function postToCodepen(sources) {
    var codePenData = {};

    if (sources.dependencies) {
        codePenData.head = _depend2['default'].renderHeadBlock(sources.dependencies);
    }

    if (sources.html) {
        codePenData.html = sources.html;
    }

    if (sources.css) {
        codePenData.css = sources.css;
    }

    if (sources.js) {
        codePenData.js = sources.js;
    }

    var jsonEncodedCodePenData = JSON.stringify(codePenData);
    (0, _formPost2['default'])('https://codepen.io/pen/define', {
        data: jsonEncodedCodePenData
    });
}

exports['default'] = {
    jsbin: postToJsBin,
    jsfiddle: postToJsFiddle,
    codepen: postToCodepen
};
module.exports = exports['default'];