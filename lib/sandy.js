'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _xhr = require('./xhr');

var _xhr2 = _interopRequireDefault(_xhr);

var _postToSandbox = require('./post-to-sandbox');

var _postToSandbox2 = _interopRequireDefault(_postToSandbox);

var _trimExtraHtmlWhitespace = require('trim-extra-html-whitespace');

var _trimExtraHtmlWhitespace2 = _interopRequireDefault(_trimExtraHtmlWhitespace);

var _es6Promise = require('es6-promise');

(0, _es6Promise.polyfill)();

function fetchIfRemote(data) {
    var promise = new Promise(function (resolve, reject) {
        if (!data) {
            resolve();
        }

        var contentIsRemote = data.hasOwnProperty('url');
        if (contentIsRemote) {
            var url = data.url;

            (0, _xhr2['default'])(url, function (responseData) {
                resolve(responseData);
            });
        } else {
            resolve(data.content);
        }
    });

    return promise;
}

function fetchRemoteSources(config, callback) {
    var staticSources = {
        dependencies: config.dependencies
    };

    var remoteCodeContentPromises = [];

    if (config.html) {
        var htmlPromise = fetchIfRemote(config.html);

        htmlPromise.then(function (html) {
            staticSources.html = (0, _trimExtraHtmlWhitespace2['default'])(html);
        });

        remoteCodeContentPromises.push(htmlPromise);
    }

    if (config.css) {
        var cssPromise = fetchIfRemote(config.css);

        cssPromise.then(function (css) {
            staticSources.css = (0, _trimExtraHtmlWhitespace2['default'])(css);
        });

        remoteCodeContentPromises.push(cssPromise);
    }

    if (config.js) {
        var jsPromise = fetchIfRemote(config.js);

        jsPromise.then(function (js) {
            staticSources.js = (0, _trimExtraHtmlWhitespace2['default'])(js);
        });

        remoteCodeContentPromises.push(jsPromise);
    }

    Promise.all(remoteCodeContentPromises).then(function () {
        callback(staticSources);
    });
}

function Sandy(config) {
    return {
        pushTo: function pushTo(destination) {
            fetchRemoteSources(config, function (staticSource) {
                _postToSandbox2['default'][destination](staticSource);
            });
        }
    };
}

exports['default'] = Sandy;
module.exports = exports['default'];