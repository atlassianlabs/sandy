'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});
function renderCssDependency(fileName) {
    return '<link rel="stylesheet" type="text/css" href="' + fileName + '"/>';
}

function renderJsDependency(fileName) {
    return '<script src="' + fileName + '"></script>';
}

function renderDependency(fileName) {
    var extension = fileName.split('.').pop();
    if (extension === 'js') {
        return renderJsDependency(fileName);
    } else if (extension === 'css') {
        return renderCssDependency(fileName);
    }
}

/**
 * Construct the head block for some list of dependencies, can be js or css.
 * @param dependencies an array of filenames to go into the head block
 * @returns {string} html just the scripts and links that would go into the HTML <head>
 */
function renderHeadBlock(dependencies) {
    var headBlock = '<!-- External dependencies -->\n';
    dependencies.forEach(function (fileName) {
        headBlock += renderDependency(fileName) + '\n';
    });
    headBlock += '<!-- / External dependencies -->';
    return headBlock;
}

/**
 * Wrap content in a head block. Currently only supports HTML (which is the only thing that needs dependencies)
 * @param contents the contents of the <body>
 * @param dependencies an array of filenames to go into the head block
 * @returns {String} html the full HTML page
 */
function wrapContentInDependencies(contents, dependencies) {
    var noDependencies = dependencies.length === 0;
    if (noDependencies) {
        return contents;
    }

    var headContents = tabBlockIn(renderHeadBlock(dependencies), 8);
    var correctlyTabbedContents = tabBlockIn(contents, 8);

    return '<html>\n' + '    <head>\n' + headContents + '\n' + '    </head>\n' + '    <body>\n' + correctlyTabbedContents + '\n' + '    </body>\n' + '</html>';
}

function tabBlockIn(block, amount) {
    var spaces = Array(amount + 1).join(' ');
    var lines = block.split('\n');
    var allLinesExceptFirstTabbedBlock = lines.join('\n' + spaces);
    var tabbedBlock = spaces + allLinesExceptFirstTabbedBlock;
    return tabbedBlock;
}

exports['default'] = {
    wrapContentInDependencies: wrapContentInDependencies,
    renderHeadBlock: renderHeadBlock
};
module.exports = exports['default'];