# Sandy

Upload code snippets to various sandboxes.

## API
```
var example = Sandy({
    dependencies: [
        'test.js',
        'test.css'
    ],
    html: {
        content: '<b>TEST</b>'
    },
    js: {
        content: 'alert(\'!\')'
    },
    css: {
        content: 'b {border: 1px solid pink;}'
    }
});
```

See the test page for a demo.

## Development

Run `npm install` and then to build the dist files `npm run prepublish`.

### External contributions

If you would like to contribute to this, please [sign the CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)
