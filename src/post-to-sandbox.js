import FormPost from './form-post';
import DependencyRenderer from './depend';

function postToJsFiddle(sources) {
    var jsFiddleData = {
        wrap: 'd'
    };

    if (sources.html) {
        var jsFiddleExternalDependencies = DependencyRenderer.renderHeadBlock(sources.dependencies);
        jsFiddleData.html = jsFiddleExternalDependencies + '\n' + sources.html;
    }

    if (sources.css) {
        jsFiddleData.css = sources.css;
    }

    if (sources.js) {
        jsFiddleData.js = sources.js;
    }

    FormPost('https://jsfiddle.net/api/post/library/pure/', jsFiddleData);
}

function postToJsBin(sources, config = {}) {
    var jsBinData = {};

    var panelsToDisplay = ['output'];

    if (sources.html) {
        jsBinData.html = DependencyRenderer.wrapContentInDependencies(sources.html, sources.dependencies);
        panelsToDisplay.push('html');
    } else {
        jsBinData.html = DependencyRenderer.wrapContentInDependencies('', sources.dependencies);
    }

    if (sources.css) {
        jsBinData.css = sources.css;
        panelsToDisplay.push('css');
    }

    if (sources.js) {
        jsBinData.javascript = sources.js;
        panelsToDisplay.push('js');
    }

    if (config && config.authToken) {
        jsBinData.api_key = config.authToken;
    }

    FormPost('https://jsbin.com/api/save', jsBinData);
}

function postToCodepen(sources) {
    var codePenData = {};

    if (sources.dependencies) {
        codePenData.head = DependencyRenderer.renderHeadBlock(sources.dependencies);
    }

    if (sources.html) {
        codePenData.html = sources.html;
    }

    if (sources.css) {
        codePenData.css = sources.css;
    }

    if (sources.js) {
        codePenData.js = sources.js;
    }

    var jsonEncodedCodePenData = JSON.stringify(codePenData);
    FormPost('https://codepen.io/pen/define', {
        data: jsonEncodedCodePenData
    });
}

export default {
    jsbin: postToJsBin,
    jsfiddle: postToJsFiddle,
    codepen: postToCodepen
};
