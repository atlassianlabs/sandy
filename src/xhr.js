export default function xhr(url, callback) {
    function reqListener () {
        callback(this.responseText);
    }

    var oReq = new XMLHttpRequest();
    oReq.onload = reqListener;
    oReq.open('get', url, true);
    oReq.send();
}