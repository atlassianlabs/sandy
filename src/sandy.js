import Xhr from './xhr';
import PostTo from './post-to-sandbox';
import TrimWhitespace from 'trim-extra-html-whitespace';
import {polyfill as es6PromisePolyfill} from 'es6-promise'

es6PromisePolyfill();

function fetchIfRemote(data) {
    var promise = new Promise(function (resolve, reject) {
        if (!data) {
            resolve();
        }

        var contentIsRemote = data.hasOwnProperty('url');
        if (contentIsRemote) {
            var url = data.url;

            Xhr(url, function (responseData) {
                resolve(responseData);
            });
        } else {
            resolve(data.content);
        }
    });

    return promise;

}

function fetchRemoteSources(config, callback) {
    var staticSources = {
        dependencies: config.dependencies
    };

    var remoteCodeContentPromises = [];

    if (config.html) {
        var htmlPromise = fetchIfRemote(config.html);

        htmlPromise.then(function(html) {
            staticSources.html = TrimWhitespace(html);
        });

        remoteCodeContentPromises.push(htmlPromise);
    }

    if (config.css) {
        var cssPromise = fetchIfRemote(config.css);

        cssPromise.then(function(css) {
            staticSources.css = TrimWhitespace(css);
        });

        remoteCodeContentPromises.push(cssPromise);
    }

    if (config.js) {
        var jsPromise = fetchIfRemote(config.js);

        jsPromise.then(function(js) {
            staticSources.js = TrimWhitespace(js);
        });

        remoteCodeContentPromises.push(jsPromise);
    }

    Promise.all(remoteCodeContentPromises).then(function () {
        callback(staticSources);
    });
}

function Sandy(config) {
    return {
        pushTo: function(destination) {
            fetchRemoteSources(config, function (staticSource) {
                PostTo[destination](staticSource);
            });
        }
    };
}

export default Sandy;
