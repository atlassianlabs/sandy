export default function(url, fields) {
    var form = document.createElement('form');
    form.setAttribute('action', url);
    form.setAttribute('method', 'post');
    form.setAttribute('style', 'display: none');
    form.setAttribute('target', '_blank');

    for (var key in fields) {
        if (fields.hasOwnProperty(key)) {
            var input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', key);
            input.setAttribute('value', fields[key]);
            form.appendChild(input);
        }
    }

    document.body.appendChild(form);
    form.submit();
};
